export const formatDate = function(date) {
			  var date = new Date(date);
			  var YY = date.getFullYear() + '-';
			  var MM = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
			  var DD = (date.getDate() < 10 ? '0' + (date.getDate()) : date.getDate());
			  var hh = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ':';
			  var mm = (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()) + ':';
			  var ss = (date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds());
			  return YY + MM + DD +" "+hh + mm + ss;
}
export const debounce = (fn, delay = 500)=> {   //默认300毫秒
	var timer;
	return function (...args) {
	    clearTimeout(timer)
	    timer = setTimeout(() => {
	      fn.apply(this, args)
	    }, delay)
	}
}
export function openMsg(id){
	var that = this;
	return new Promise((resolve, reject)=>{
		wx.getSetting({
		 	// withSubscriptions: true,//是否获取用户订阅消息的订阅状态，默认false不返回
		   success(res){
		   if(res.authSetting['scope.subscribeMessage']){
			   let params = {
				   code:1
			   }
			   console.log('1',res)
			   resolve(params);
		     // uni.openSetting({ // 打开设置页
		     //   success(res) {
		     //   console.log(res.authSetting)
		     //                            }
		     // });
		   }else{// 用户没有点击“总是保持以上，不再询问”则每次都会调起订阅消息
		     uni.requestSubscribeMessage({
		       tmplIds: id,// 
		       success (res) {
				   let params = {
					   code:2,
					   res
				   }
				   resolve(params);
		         console.log('2',res)
		         // if(res['字段'] == "accept"){// 字段就是tmplIds模板id
		         //     // ....
		         // }
		       },
			   fail(err){
				   console.log('3',res)
				  reject(err) 
			   }
		      })
		     }
		   }
		})
		// uni.requestSubscribeMessage({
		//   tmplIds:id,// 
		//   success (res) {
		//     // console.log(res)
		//     // if(res['字段'] == "accept"){// 字段就是tmplIds模板id
		//     //     // ....
		//     // }
		//   },
		//   complete(er){
		//     resolve(er)
		//   }
		//  })
	})
}