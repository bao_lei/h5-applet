import Vue from 'vue';
// #ifdef H5
import wechat from '@/utils/js/wechat.js';
Vue.prototype.$wechat = wechat
// #endif
import loading from '@/components/loading/loading.vue';
Vue.component('loading',loading)

Vue.prototype.toast = (msg) => {
	uni.showToast({
		title: msg,
		duration: 2000,
		position: 'bottom',
		icon: 'none'
	});
};

import {mixin} from '@/utils/mixin/mixin.js';
Vue.mixin(mixin)