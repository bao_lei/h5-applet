export function ajax(url, data) {
	// let token = "";
	// uni.getStorage({
	// 	key: 'token',
	// 	success: function(ress) {
	// 		token = ress.data
	// 	}
	// });
	
	var temp_data = data;
	if (temp_data == undefined) temp_data = {};
	const res = uni.getSystemInfoSync();
	//  // console.log (res)
	// 运行在小程序中
	if (res.version) {
		// this.login();
		var urls = 'https://www.saixiaoli.com/phone/' + url;
	} else {
		// this.html_login()
		var urls = '/phone/' + url;
	}
    //此token是登录成功后后台返回保存在storage中的
	// var header = {
	// 	'Content-Type': 'application/json',
	// 	'Token':uni.getStorageSync('token'),
	// 	'Cityname':uni.getStorageSync('city'),
	// 	'Provincename':uni.getStorageSync('province'),
	// 	'Latitude':uni.getStorageSync('latitude'),
	// 	'Longitude':uni.getStorageSync('longitude'),
	// };
	// #ifdef MP-WEIXIN
		const accountInfo = uni.getAccountInfoSync();
		const AppId = accountInfo.miniProgram.appId;
	// #endif
    let httpDefaultOpts = {
        url: urls,
        data: temp_data,
        method:'POST',
        header:{
			'Content-Type': 'application/json',
			'Token':uni.getStorageSync('token'),
			'Cityname':uni.getStorageSync('city'),
			'Provincename':uni.getStorageSync('province'),
			'Latitude':uni.getStorageSync('latitude'),
			'Longitude':uni.getStorageSync('longitude'),
			// #ifdef MP-WEIXIN
				'AppId':AppId
			// #endif
		}
    }
    let promise = new Promise(function(resolve, reject) {
        uni.request(httpDefaultOpts).then(
            (res) => {
				// console.log(res)
				if(res[1].data.code == 4) {
					if(!uni.getSystemInfoSync().version){
					  // window.location ='https://sxl.rxzly.cc';
					  window.location ='https://www.saixiaoli.com/pages/';
					  return false;
					} else {
						uni.removeStorageSync('token');
						// let routes = getCurrentPages();
						// let curRoute = routes[routes.length - 1].route
						// let options = routes[routes.length - 1].options;
						// var url_str = '';
						// for(var i in options) {
						//   if(!url_str) url_str += '?'+i+'='+options[i];
						//   else url_str += '&'+i+'='+options[i];
						// }
						// var router_str = curRoute + url_str;
						// uni.reLaunch({
						//   url:'/pagesA/login/login?url=/'+encodeURIComponent(router_str)
						// })
					}
					
				}else{}
				if(res[1].data.code == 2){
					// console.log('ddd5555')
				   uni.reLaunch({
				   	url:'/pages/erro?msg='+res[1].data.msg
				   })
				   return;
				}
				if(res[1].data.code == 3){
					// let token = this.$store.state.tabbar.token;
					// console.log(that.$store)
					if(!uni.getStorageSync('quToken')){
						window.location ='https://www.saixiaoli.com/pages/';
						return;
					}
					window.location ='https://www.saixiaoli.com/pages/index/index/token/'+uni.getStorageSync('quToken');
					return;
					// console.log('ddd5555')
				}
				resolve(res[1].data)
            }
        ).catch(
            (response) => {
                reject(response)
            }
        )
    })
    return promise
};