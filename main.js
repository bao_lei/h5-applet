import Vue from 'vue'
import App from './App'
import minRouter from './router/router.js';
import MinRouter from './router/MinRouter.js';
import store from '@/store/index.js';
import gatherMth from '@/utils/js/index.js';

Vue.use(MinRouter)
Vue.config.productionTip = false

App.mpType = 'app'

const app = new Vue({
	minRouter,
	store,
	gatherMth,
    ...App
})
app.$mount()
