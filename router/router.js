import MinRouter from './MinRouter.js'
// 配置路由
const router = new MinRouter({
  routes: [
    {
      // 首页
      path: 'pages/index/index',
      name: 'index'
    }
  ]
 })
export default router