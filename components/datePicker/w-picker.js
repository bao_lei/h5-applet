const forMatNum=(num)=>{
	return num<10?'0'+num:num+'';
}
const initPicker={
	//日期
	date:{
		init(start,end,mode,step,value,flag,disabled,s,e,moDetaY){
			// console.log('初始拿值',start,end,mode,step,value,flag,disabled,s,e,moDetaY)
			var moDetaYear = moDetaY ? moDetaY : 1949;
			var aToday = 0;
			var sToday = 0;
			if(s && e){
				aToday=new Date(e);
				sToday =new Date(s)
				// console.log('存在')
			}else{
				aToday=new Date();
				// console.log('不存在')
				// console.log(aToday)
			}
			// sToday=new Date();
			// console.log(aToday)
			
			// let sToday =new Date(1590940800000)
			// let tYear,tMonth,tDay,tHours,tMinutes,tSeconds,defaultVal=[];
			let tYear,tMonth,tDay,defaultVal=[];
			let initstartDate=new Date(start.toString());
			// console.log('1',initstartDate)
			let endDate=new Date(end.toString());
			if(start>end){
				initstartDate=new Date(end.toString());
				// console.log('2',initstartDate)
				endDate=new Date(start.toString());
			};
			endDate=new Date();
			let startYear=sToday ? sToday.getFullYear() : moDetaYear;
			// console.log('3',startYear)
			let startMonth=sToday ? sToday.getMonth()+1 : 1;
			// console.log('初始月份',startMonth)
			let endYear=endDate.getFullYear();
			let years=[],months=[],days=[],hours=[],minutes=[],seconds=[],areas=[],returnArr=[];
			// let years=[],months=[],days=[],returnArr=[];
			let curMonth=flag?value[1]*1:(value[1]+1);
			let dYear=aToday.getFullYear();
			let sYear = sToday ? sToday.getFullYear() : moDetaYear
			let dMonth=aToday.getMonth()+1;
			let sMonth =sToday ? sToday.getMonth()+1 : 1;
			let dDate=aToday.getDate();
			let sDate =sToday ? sToday.getDate() : 1;
			let totalDays=new Date(startYear,curMonth,0).getDate();
			let dvalObj={};
			switch(mode){
				case "date":
					let curYear=value[0];
					let curMonth=value[1];
					if(disabled){
						for(let s=startYear;s<=dYear;s++){
							years.push(s+'');
						};
						let totalDaysAbl=new Date(startYear,startMonth,0).getDate();
						// console.log('年份为啥一样',+startYear,+dYear)
						if(+startYear == +dYear){
							// console.log('初始当前年份和结束年份相同')
							// console.log('初始当前年份和结束年份相同','sMonth == 1')
							for(let m=1;m<=dMonth;m++){
								months.push(forMatNum(m));
							};
							// if(sMonth == 1){
							// 	console.log('初始当前年份和结束年份相同','sMonth == 1')
							// 	for(let m=sMonth;m<=dMonth;m++){
							// 		months.push(forMatNum(m));
							// 	};
							// }else{
							// 	console.log('初始当前年份和结束年份相同','sMonth != 1')
							// 	for(let m=1;m<=dMonth;m++){
							// 		months.push(forMatNum(m));
							// 	};
							// }
						}else if(+startYear == +sYear){
							// console.log('初始当前年份和开始年份相同','sMonth == 1')
							for(let m=sMonth;m<=12;m++){
								months.push(forMatNum(m));
							};
							// if(sMonth == 1){
							// 	console.log('初始当前年份和开始年份相同','sMonth == 1')
							// 	for(let m=sMonth;m<=12;m++){
							// 		months.push(forMatNum(m));
							// 	};
							// }else{
							// 	console.log('初始当前年份和开始年份相同','sMonth != 1')
							// 	for(let m=1;m<=dMonth;m++){
							// 		months.push(forMatNum(m));
							// 	};
							// }
						}
						// }else if(mode == 'date'){
						// 	if(sMonth == 1){
						// 		console.log('时间','sMonth == 1')
						// 		for(let m=sMonth;m<=dMonth;m++){
						// 			months.push(forMatNum(m));
						// 		};
						// 	}else{
						// 		console.log('时间','sMonth == 1')
						// 		for(let m=1;m<=dMonth;m++){
						// 			months.push(forMatNum(m));
						// 		};
						// 	}
						// }
						else{
							// console.log('都不想等','sMonth == 1')
							for(let m=1;m<=12;m++){
								months.push(forMatNum(m));
							};
						}
						// console.log('当前选中的月份和开始月份',startMonth,sMonth)
						if(+startMonth == +sMonth && +startYear == +sYear){
							// console.log('开始年份相同，开始月份相同',sDate,startMonth,sMonth,startYear,sYear)
							for(let d=sDate;d<=totalDaysAbl;d++){
								days.push(forMatNum(d));
							}
							// if(sDate == 1){
							// 	for(let d=sDate;d<=totalDays;d++){
							// 		days.push(forMatNum(d));
							// 	}
							// }else{
							// 	for(let d=1;d<=dDate;d++){
							// 		days.push(forMatNum(d));
							// 	}
							// }
						}else if(+startMonth == +dMonth && +startYear == +dYear){
							// console.log('结束年份相同，结束月份相同',dDate)
							for(let d=1;d<=dDate;d++){
								days.push(forMatNum(d));
							}
						}
						// else if(){
							
						// }
						// else if(mode == 'date'){
						// 	if(sDate == 1){
						// 		for(let d=sDate;d<=dDate;d++){
						// 			days.push(forMatNum(d));
						// 		}
						// 	}else{
						// 		for(let d=1;d<=dDate;d++){
						// 			days.push(forMatNum(d));
						// 		}
						// 	}
						// }
						else{
							// console.log('都不相同，月份不相同',totalDaysAbl)
							// console.log('当前月份',totalDays)
							for(let d=1;d<=totalDaysAbl;d++){
								days.push(forMatNum(d));
							}
						}
						
					}else{
						// console.log('进这里了？',startYear,endYear)
						for(let s=startYear;s<=endYear;s++){
							years.push(s+'');
							 // console.log (startYear);
						};
						for(let m=1;m<=12;m++){
							months.push(forMatNum(m));
						};
						for(let d=1;d<=totalDays;d++){
							days.push(forMatNum(d));
						}
					};
					break;
				default:
					for(let s=startYear;s<=endYear;s++){
						years.push(s+'');
					};
					for(let m=1;m<=12;m++){
						months.push(forMatNum(m));
					};
					for(let d=1;d<=totalDays;d++){
						days.push(forMatNum(d));
					}
					break;
			}
			for(let h=0;h<24;h++){
				hours.push(forMatNum(h));
			}
			for(let m=0;m<60;m+=step*1){
				minutes.push(forMatNum(m));
			}
			for(let s=0;s<60;s++){
				seconds.push(forMatNum(s));
			}
			if(!flag){
				returnArr=[
					years.indexOf(value[0]),
					months.indexOf(value[1]),
					days.indexOf(value[2]),
					// hours.indexOf(value[3]),
					// minutes.indexOf(value[4])==-1?0:minutes.indexOf(value[4]),
					// seconds.indexOf(value[5])
				]
				// console.log('大胆地对待',returnArr)
			}
			switch(mode){
				case "date":
					if(flag){
						defaultVal=[returnArr[0],returnArr[1],returnArr[2]];
						// console.log('date默认选中1',defaultVal)
						return {years,months,days,defaultVal}
					}else{
						defaultVal=[
							// years.indexOf(value[0])==-1?0:years.indexOf(value[0]),
							// months.indexOf(value[1])==-1?0:months.indexOf(value[1]),
							// days.indexOf(value[2])==-1?0:days.indexOf(value[2])
							years.length-1,
							months.length-1,
							days.length-1
						];
						let ye = new Date(e);
						// let = ye.getFullYear();
						months = this.initMonths(ye.getFullYear(),true,s,e)
						days = this.initDays(ye.getFullYear(),months[months.length-1],true,s,e)
						// console.log('date默认选中2',years,months,days.length-1)
						return {years,months,days,defaultVal}
					}
					break;
			}
		},
		initMonths:(year,disabled,ss,ee)=>{
			var aDate = 0;
			var sDate = 0;
			if(ss && ee){
				aDate=new Date(ee);
				sDate = new Date(ss);
			}else{
				aDate=new Date()
			}
			let dYear=aDate.getFullYear();
			let sYear =sDate ? sDate.getFullYear() : 0;
			let dMonth=aDate.getMonth()+1;
			let sMonth =sDate ? sDate.getMonth()+1 : 0;
			let dDate=aDate.getDate();
			let ssDate =sDate ? sDate.getDate() : 0;
			let flag=dYear==year?true:false;
			// let yearFlag = year == sYear ? true : false; 
			let months=[];
			if(disabled){
				if(ss && ee){
					var yearFlag = 0;
					if(year == sYear){
						yearFlag = 1;
					}
					if(year == dYear){
						yearFlag = 2;
					}
					if(year != dYear && year != sYear){
						yearFlag = 3;
					}
					if(yearFlag == 1){
						for(let m=sMonth;m<=12;m++){
							months.push(forMatNum(m));
						};
					}else if(yearFlag == 2){
						for(let m=1;m<=dMonth;m++){
							months.push(forMatNum(m));
						};
					}else{
						for(let m=1;m<=12;m++){
							months.push(forMatNum(m));
						};
					}
					// console.log('当前月份',months)
				}else{
					if(flag){
						for(let m=1;m<=dMonth;m++){
							months.push(forMatNum(m));
						};
					}else{
						for(let m=1;m<=12;m++){
							months.push(forMatNum(m));
						};	
					}
				}
			}else{
				for(let m=1;m<=12;m++){
					months.push(forMatNum(m));
				};
			};
			return months;
		},
		initDays:(year,month,disabled,ss,ee)=>{
			// console.log('天数变化',year,month,disabled,ss,ee)
			var aDate = 0;
			var sDate = 0;
			if(ss && ee){
				aDate=new Date(ee);
				sDate = new Date(ss);
			}else{
				aDate=new Date()
			}
			// let aDate=new Date();
			let dYear=aDate.getFullYear();
			let sYear =sDate ? sDate.getFullYear() : 0;
			let dMonth=aDate.getMonth()+1;
			let sMonth =sDate ? sDate.getMonth()+1 : 0;
			let dDate=aDate.getDate();
			let ssDate =sDate ? sDate.getDate() : 0;
			let flag=(dYear==year&&dMonth==month)?true:false;
			let totalDays=new Date(year,month,0).getDate();
			let dates=[];
			if(ss && ee){
				var dayFlag = 0;
				if(month == sMonth && year == sYear){
					dayFlag = 1;
				}
				if(month == dMonth && year == dYear){
					dayFlag = 2;
				}
				if(month != sMonth && month != dMonth){
					dayFlag = 3;
				}
				if(dayFlag == 1){
					// console.log(sMonth,month,year,sYear)
					for(let d=ssDate;d<=totalDays;d++){
						dates.push(forMatNum(d));
					};	
				}else if(dayFlag == 2){
					for(let d=1;d<=dDate;d++){
						dates.push(forMatNum(d));
					};
				}else{
					for(let d=1;d<=totalDays;d++){
						dates.push(forMatNum(d));
					};
				}
			}else{
			  if(flag&&disabled){
			  	for(let d=1;d<=dDate;d++){
			  		dates.push(forMatNum(d));
			  	};			
			  }else{
				// console.log('当前年份不一样')
			  	for(let d=1;d<=totalDays;d++){
			  		dates.push(forMatNum(d));
			  	};
			  };	
			}
			return dates;
		},
	},
	//选择区间初始化
	range:{
		init(start,end,value,flag){
			let aToday=new Date();
			let tYear,tMonth,tDay,tHours,tMinutes,tSeconds,defaultVal=[];
			let initstartDate=new Date(start.toString());
			let endDate=new Date(end.toString());
			if(start>end){
				initstartDate=new Date(end.toString());
				endDate=new Date(start.toString());
			};
			let startYear=initstartDate.getFullYear();
			let startMonth=initstartDate.getMonth()+1;
			let endYear=endDate.getFullYear();
			let fyears=[],fmonths=[],fdays=[],tyears=[],tmonths=[],tdays=[],returnArr=[];
			let curMonth=flag?value[1]*1:(value[1]+1);
			let totalDays=new Date(startYear,curMonth,0).getDate();
			for(let s=startYear;s<=endYear;s++){
				fyears.push(s+'');
			};
			for(let m=1;m<=12;m++){
				fmonths.push(forMatNum(m));
			};
			for(let d=1;d<=totalDays;d++){
				fdays.push(forMatNum(d));
			};
			for(let s=startYear;s<=endYear;s++){
				tyears.push(s+'');
			};
			for(let m=1;m<=12;m++){
				tmonths.push(forMatNum(m));
			};
			for(let d=1;d<=totalDays;d++){
				tdays.push(forMatNum(d));
			};
			defaultVal=[
				fyears.indexOf(value[0])==-1?0:fyears.indexOf(value[0]),
				fmonths.indexOf(value[1])==-1?0:fmonths.indexOf(value[1]),
				fdays.indexOf(value[2])==-1?0:fdays.indexOf(value[2]),
				0,
				tyears.indexOf(value[4])==-1?0:tyears.indexOf(value[4]),
				tmonths.indexOf(value[5])==-1?0:tmonths.indexOf(value[5]),
				tdays.indexOf(value[6])==-1?0:tdays.indexOf(value[6])
			];
			return {
				fyears,
				fmonths,
				fdays,
				tyears,
				tmonths,
				tdays,
				defaultVal
			}
		},
		initDays(year,month){
			let totalDays=new Date(year,month,0).getDate();
			let dates=[];
			for(let d=1;d<=totalDays;d++){
				dates.push(forMatNum(d));
			};
			return dates;
		}
	}
}

export default initPicker