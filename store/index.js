import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
import active from './modules/active.js';

const store = new Vuex.Store({
	modules: {
		active
	}
})

export default store
