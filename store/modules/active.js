import store from '@/store'
import {
	ACTIVEINFO,
	INFO
} from '@/store/type.js';
const state = {
	stemInfo:{},
}
const actions = {
	setInfo({
		commit,
		state,
		dispatch
	},info){
		return new Promise((resolve, reject) => {
			commit('INFO',info);
		})
	}
}
const mutations = {
	[INFO](state,data){
		state.stemInfo = data;
	},
}
export default {
	state,
	mutations,
	actions
}